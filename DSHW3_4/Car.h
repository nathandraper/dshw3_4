#ifndef CAR_H
#define CAR_H

#include <string>

#include "LinkedList.h"
#include "Passenger.h"

const int NUM_FLOORS = 6;
const int CAR_SPEED = 3; // assume it takes three seconds to move another floor

class Car
{
private:
	int position; // position will be an int from 0-2 as it takes 3 seconds to move to another floor.
	int timer;
	int location;
	int maxWeight;
	double weight;
	std::string direction;
	LinkedList<Passenger> passengers[NUM_FLOORS];
	Node<std::string>* state;
	
public:
	Car();
	Car(int start, int maxWeight, Node<std::string>* init);
	
	void setTimer(int secs);
	void tickTimer();

	Passenger* unloadPassenger();
	void loadPassenger(Passenger*);

	std::string getDirection() { return this->direction; }
	void setDirection(std::string direction) { this->direction = direction; }
	
	int getLocation() { return this->location; }
	void setLocation(int location) { this->location = location; }
	void incLocation() { this->location++; }

	double getWeight() { return this->weight; }
	void addWeight(double weight) { this->weight += weight; }

	double getMaxWeight() { return this->maxWeight; }
	
	std::string getState() { return *this->state->data; }
	void nextState() { this->state = this->state->next; }

	int getPosition() { return this->position; }
	
	int readTimer() { return this->timer; }

	void incPosition() { this->timer = (timer + 1) % 3; }

};

#endif

