#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <string>
#include <map>
#include <utility>

#include "LinkedList.h"
#include "Car.h"
#include "Floor.h"
#include "Passenger.h"
#include "Clock.h"
#include "RequestQueue.h"

const int DEFAULT_CAR_NUM = 1;
const int DEFAULT_FLOOR_NUM = 6;
const int DEFAULT_HRS = 12;


class Controller
{
private:
	int numCars;
	int numFloors;
	Car* cars;
	Floor* floors;
	Clock clock;
	RequestQueue requests;
	LinkedList<std::string> states;
	map<int, Passenger*> passengers;
	map<int, pair<int, int>> schedule;


public:
	Controller(map<int, Passenger*> passengers, map<int, pair<int, int>> schedule, int numCars = DEFAULT_CAR_NUM, int numFloors = DEFAULT_FLOOR_NUM, int hrsInDay = DEFAULT_HRS, int minsInDay = 0, int secsInDay = 0);
	bool updateSim();
	void calculateCarState(int car);
	void loadPassenger(Passenger* passenger, int car);
	void callCar(int floor, std::string direction);
	void pressButton(int floor);
};

#endif

